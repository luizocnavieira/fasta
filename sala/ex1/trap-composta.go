package main

import (
	"fmt"
)

func f(x float64) float64 {
	return 10*x - x*x
}

func integralF(menor, maior, n float64) float64 {
	resultado := 0.0
	h := (maior - menor) / n
	x := menor

	for i := 1.0; i < n; i++ {
		resultado = resultado + f(x) + f(x+h)
		x = x + h
	}
	resultado = resultado * h / 2
	return resultado
}

func main() {
	var menor, maior, n float64

	fmt.Print("limite menor: ")
	fmt.Scan(&menor)
	fmt.Print("limite maior: ")
	fmt.Scan(&maior)
	fmt.Print("intervalo: ")
	fmt.Scan(&n)

	fmt.Println(integralF(menor, maior, n))
}
