#include <stdio.h>
#include <math.h>


double f(double  x) {
    return 10*x-x*x;
}

double integral_de_f(double limite_menor, double limite_maior, double  n) {
    double resultado = 0;
    double h = (limite_maior - limite_menor) / n;
    double x = limite_menor;

    for (int i = 1; i < n; i++) {
    resultado = resultado + f(x) + f(x + h);
    x = x + h;
    }
    resultado = resultado * h / 2;
    return resultado;
}


int main() {
    int n;
    double limite_menor, limite_maior;

    printf("limite menor: ");
    scanf("%lf", &limite_menor);
    printf("limite maior: ");
    scanf("%lf", &limite_maior);
    printf("intervalo: ");
    scanf("%d", &n);
    printf("%lf", integral_de_f(limite_menor, limite_maior, n));
    
    return 0; 
}