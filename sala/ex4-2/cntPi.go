package main

import (
	"fmt"
	"math/rand"
	utils "parallel/commons"
	"time"
)

const PI = 3.141592653589

func estimatePi(i int, n float64, c chan float64) {
	rand.Seed(int64(time.Now().UnixNano()))

	cnt := 0.0
	for j := 0.0; j < n; j++ {
		x := rand.Float64()
		y := rand.Float64()
		z := x*x + y*y
		if z <= 1.0 {
			cnt++
		}
	}
	fmt.Println("Core", i, "identificou", cnt, "amostras dentro dessa circunferência")
	c <- cnt
}

func main() {
	numOfThreads, _ := utils.ParseFlags()
	c := make(chan float64)
	np := float64(numOfThreads)

	var entrada int
	fmt.Print("Numero de amostras desejadas: ")
	fmt.Scan(&entrada)
	n := float64(entrada)
	fraction := n / np

	var sum float64
	go estimatePi(0, fraction, c)
	sum = <-c

	for i := 1; i < numOfThreads; i++ {
		go estimatePi(i, fraction, c)
		x := <-c
		sum = sum + x
	}

	est4pi := 4 * sum / n
	fmt.Println("Estimativa final de Pi", est4pi)
	fmt.Printf("erro: %.3f", est4pi-PI)
}
