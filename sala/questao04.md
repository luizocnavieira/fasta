# Questão 4

Os programas estão utilizando o método de Monte Carlo para estimar o valor de π. Esse método consiste em jogar objetos aleatoriamente numa área quadrada em que um círculo está circunscrito. Ao observar a proporção de objetos que caíram dentro do círculo, é possível estimar π.

Mais precisamente, pensemos no plano cartesiano. Imagine um quadrado cujos vértices estão nas coordenadas (1, 1), (1, -1), (-1, -1) e (-1, 1). Cada lado desse quadrado terá comprimento de 2u.m, portanto sua área será de 4u.m². Agora imagine um círculo circunscrito nesse quadrado, centrado na origem do plano cartesiano e com raio de 1u.m. Sua área será πu.m²

Dessa forma, se considerarmos que todos os objetos são lançados de forma completamente aleatória, podemos dizer que a chance de um objeto cair dentro do círculo é proporcional à área deste. Ora, quanto maior o círculo, mais chances tem um objeto de cair nele. Daí vem que o número de objetos que caem dentro do círculo está para π, como o total de objetos lançados está para 4. Com isso obtemos que π é o quádruplo da razão do número de objetos que caíram dentro do círculo pelo número de objetos lançados.

O que a função `estimate_pi` está fazendo é estimar π lançando `n` objetos, ou melhor, escolhendo `n` pares ordenados ao acaso.
```c
double estimate_pi ( int i, int n )
{
   srand(time(NULL)+i);

   int j,cnt=0;
   for(j=0; j<n; j++)
   {
      double x = ((double) rand() / (RAND_MAX));
      double y = ((double) rand() / (RAND_MAX));
      double z = x*x + y*y;

      if(z <= 1.0) cnt++;
   }
 
   double estimate = (4.0*cnt)/n;
 
   printf("Core %d estima Pi em : %.15f",i,estimate);
   printf("  erro : %.3e\n",fabs(estimate-PI));

   return estimate;
}
```
São gerados `n` pares aleatoriamente. Para saber se um par pertence ao círculo, basta verificar se `x² + y² <= 1`, expressão trivialmente derivada pelo teorema de pitágoras. Dessa forma, a variável `n` indica o total de pontos gerados, enquanto `cnt` armazena o número de pontos que eram pertencentes ao círculo. Com isso, é retornada a estimativa `4 * cnt/n`.

Os dois programas fazem a mesma coisa: na função main, há a inicialização do MPI. O processo 0 fica reponsável por ler o número de amostras para a estimativa, que é então enviado via broadcast para os demais. Por fim, o processo 0 obtém a estimativa média e exibe na saída padrão. 

Porém, há uma sutil diferença.

Seja P o número de processos e N o valor lido da entrada padrão. No programa A, N é transmitido via broadcast e cada processo coleta N amostras de pares ordenados, de modo que, ao total, são obtidas P*N amostras. Já no programa B, é transmitido via broadcast N/P, de modo que ao total são obtidas N amostras. No programa A, a função de estimativa retorna uma estimativa de π, enquanto que no programa B essa função retorna o número de amostras dentro do círculo, ficando a cargo do processo 0 estimar π a partir desses valores.