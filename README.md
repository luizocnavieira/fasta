# Avaliação II - Grupo E
Alunos participantes:
Allan Juan Silva Araujo |
Victor Matheus Sobral Chagas |
Wallace Hora Lessa Neto |
Caio Gabriel Bispo dos Santos |
Luiz Gustavo da Silva Vieira 

Para este trabalho, optamos por solucionar os problemas propostos nas fichas de estudo. A seguir estão todas as informações relevantes.

## Instalação e Uso

Primeiro, clone o repositório. 
```
// Clonando o repositório
$ git clone https://bitbucket.org/luizocnavieira/fasta/src/master/

$ cd master
```

Para executar os programas, você tem duas opções: compilar _on the fly_ com `go run`, ou compilar com `go build` e depois rodar o executável. Vamos exemplificar cada uma delas.

```
// Compilando e executando em um só comando
$ go run fe04/fatorial.go
Calculating (7, 10)
Calculating (4, 7)
Calculating (0, 4)
3628800
```

```
// Compilando primeiro
$ go build fe04/fatorial.go

// Executando
$ ./fe04/fatorial
Calculating (7, 10)
Calculating (0, 4)
Calculating (4, 7)
3628800
```

Além disso, é possível passar flags para modificar o número de processos e o tamanho do problema. Por padrão, esses valores serão 4 e 10, respectivamente.
```
// Mudando o tamanho do problema
$ go run fe04/fatorial.go -n=12
Calculating (8, 12)
Calculating (0, 4)
Calculating (4, 8)
479001600

// Mudando o número de processos
$ go run fe04/fatorial.go -np=6
Calculating (8, 10)
Calculating (6, 8)
Calculating (0, 2)
Calculating (2, 4)
Calculating (4, 6)
3628800

// Mudando ambos
$ go run fe04/fatorial.go -n=8 -np=3
Calculating (4, 8)
Calculating (0, 4)
40320
```

A ordem em que as flags são fornecidas não importa.