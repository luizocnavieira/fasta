package commons

import (
	"flag"
	"fmt"
	"os"
)

func ParseFlags() (int, int, int, int) {
	numOfThreadsPointer := flag.Int("np", 4, "number of threads")
	nPointer := flag.Int("n", 10, "problem size")
	lowerBoundPointer := flag.Int("a", 0, "lower bound")
	upperBoundPointer := flag.Int("b", 10, "upper bound")

	flag.Parse()

	n := *nPointer
	numOfThreads := *numOfThreadsPointer
	lowerBound := *lowerBoundPointer
	upperBound := *upperBoundPointer

	if n < 1 || numOfThreads < 2 {
		fmt.Println("Invalid arguments. n must be positive and np must be greater or equal than 2")
		os.Exit(1)
	}

	if lowerBound > upperBound {
		fmt.Println("Invalid arguments. Lower bound must be smaller than upper bound")
		os.Exit(1)
	}

	return numOfThreads, n, 1, 2
}