package main

import (
	"fmt"
	utils "parallel/commons"
)

func factorial(beginning int, end int, channel chan int) {
	fmt.Printf("Calculating (%d, %d)\n", beginning, end)

	product := 1

	for i := beginning + 1; i <= end; i++ {
		product = product * i
	}

	channel <- product
}

func main() {
	numOfThreads, n, _, _ := utils.ParseFlags()

	channel := make(chan int)
	taskSize := n / (numOfThreads - 1)
	offset := n % (numOfThreads - 1)

	for i := 1; i < numOfThreads; i++ {
		beginning := (i - 1) * taskSize
		end := beginning + taskSize + offset

		if i != 1 {
			beginning += offset
		}

		go factorial(beginning, end, channel)
	}

	factorial := 1

	for i := 1; i < numOfThreads; i++ {
		receivedValue := <-channel
		factorial = factorial * receivedValue
	}

	fmt.Println(factorial)
}


