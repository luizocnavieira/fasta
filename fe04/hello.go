package main

import (
	"fmt"
	utils "parallel/commons"
	"sync"
)

func main() {
	numOfThreads, _, _, _ := utils.ParseFlags()
	var waitGroup sync.WaitGroup

	for i := 0; i < numOfThreads; i++ {
		// Indica que uma nova goroutine será executada.
		waitGroup.Add(1)

		go func(myRank int, numOfThreads int) {
			fmt.Printf("Hello from process %d of %d!\n", myRank, numOfThreads);
			
			// Indica que uma goroutine foi concluída.
			waitGroup.Done()
		}(i, numOfThreads)
	}

	// Garante que o programa só termina após executar todas as goroutines.
	waitGroup.Wait()
}