package main

import (
	"fmt"
	utils "parallel/commons"
)

func sayHello(i int, numOfThreads int, channel chan string) {
	channel <- fmt.Sprintf("Hello from process %d of %d!\n", i, numOfThreads)
}

func main() {
	numOfThreads, _, _, _ := utils.ParseFlags()
	channel := make(chan string)

	for i := 1; i < numOfThreads; i++ {
		go sayHello(i, numOfThreads, channel)
	}

	fmt.Printf("Hello from process 0 of %d!\n", numOfThreads)

	for i := 1; i < numOfThreads; i++ {
		message := <-channel
		fmt.Print(message)
	}
}