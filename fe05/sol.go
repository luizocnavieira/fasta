package main

import (
	"fmt"
	utils "parallel/commons"
)

func pow(x float64) float64 {
	return x * x
}

func Simps(a float64, b float64, n float64) float64 {
	var sum1, sum2 float64
	h := (b - a) / n
	y0 := pow(a + 0*h)
	yn := pow(a + n*h)
	for i := 1; i < int(n); i++ {
		if i%2 == 0 {
			sum1 = sum1 + pow(a+float64(i)*h)
		} else {
			sum2 = sum2 + pow(a+float64(i)*h)
		}
	}
	sum := (h / 3) * (y0 + yn + 2*sum1 + 4*sum2)
	return sum
}

func calculate(n int, np int, h float64, i int, channel chan float64) {
	localN := float64(n / np)
	localA := 0 + float64(i)*h
	localB := localA + h

	localPi := Simps(localA, localB, localN)

	channel <- localPi
}

func main() {
	numOfThreads, n, _, _ := utils.ParseFlags()

	channel := make(chan float64)

	var h float64 = float64(1.0 / numOfThreads)

	for i := 1; i < numOfThreads; i++ {
		go calculate(n, numOfThreads, h, i, channel)
	}

	var resul float64

	for i := 1; i < numOfThreads; i++ {
		value := <-channel
		resul = resul + value
	}

	fmt.Println(resul)
}
