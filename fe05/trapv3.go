package main

import (
	"fmt"
	utils "parallel/commons"
)

func f(x float64) float64 {
	return x * x
}

func Trap(leftBound float64, rightBound float64, trapCount float64, baseLength float64) float64 {
	estimate := (f(leftBound) + f(rightBound)) / 2.0

	for i := 1; i < int(trapCount); i++ {
		x := leftBound + (float64(i) * baseLength)
		estimate = estimate + f(x)
	}
	estimate = estimate * baseLength
	return estimate
}

func calc(a float64, b float64, localN float64, h float64, i float64, channel chan float64) {
	localA := a + i*localN*h
	localB := localA + localN*h
	localInt := Trap(localA, localB, localN, h)
	channel <- localInt
}

func reduce(channel chan float64, n int) float64 {
	var res float64
	for i := 1; i <= n; i++ {
		value := <-channel
		res = res + value
	}
	return res
}

func main() {
	numOfThreads, n, aInt, bInt := utils.ParseFlags()

	a := float64(aInt)
	b := float64(bInt)

	h := (b - a) / float64(n)
	localN := n / numOfThreads

	channel := make(chan float64)

	for i := 1; i <= numOfThreads; i++ {
		go calc(a, b, float64(localN), float64(h), float64(i), channel)
	}

	total := reduce(channel, numOfThreads)

	fmt.Printf("Com n = %d trapezoides, nossa estimativa \n", n)
	fmt.Printf("da integral de %f e %f = %.15e \n", a, b, total)
}
